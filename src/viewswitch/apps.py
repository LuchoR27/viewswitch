from django.apps import AppConfig


class ViewswitchConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'viewswitch'
