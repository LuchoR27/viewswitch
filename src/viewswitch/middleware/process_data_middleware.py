import ast
from urllib import parse


def process_data_middleware(get_response):

    def middleware(request):
        data = None
        if request.method == 'GET':
            if 'HTTP_DATA' in request.META:
                data = parse.parse_qs(request.META['HTTP_DATA'])
            elif request.GET:
                data = request.GET
            elif not request.session.is_empty():
                data = {}
                for key, val in request.session.items():
                    data[key] = val
            else:
                data = request.COOKIES
        elif request.method == 'POST':
            if request.content_type == "application/json":
                data = ast.literal_eval(request.body.decode("utf-8"))
            else:
                data = request.POST
        elif request.method == 'PUT':
            data = ast.literal_eval(request.body.decode("utf-8"))
        request.session['data'] = data
        response = get_response(request)
        return response

    return middleware
