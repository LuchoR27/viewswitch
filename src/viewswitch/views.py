import ast
import json

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, FileResponse, HttpResponseNotFound
from django.shortcuts import redirect
from django.urls import reverse


def myswitchview(request):
    data = request.session['data']
    response = HttpResponse(data)
    if 'Accept' in request.headers:
        if request.headers['Accept'] == 'text/csv':
            with open('response.csv', 'w') as csvfile:
                for key in data.keys():
                    csvfile.write("%s, %s\n" % (key, data[key]))
                response = FileResponse(open('response.csv', 'rb'))
        if request.headers['Accept'] == 'application/json':
            response = JsonResponse(data, safe=False)
    return response


def myswitchview2(request):
    if request.method == 'GET':
        data = request.session['data']
        response = HttpResponse(data)
        if 'Accept' in request.headers:
            if request.headers['Accept'] == 'text/csv':
                with open('response.csv', 'w') as csvfile:
                    for key in data.keys():
                        csvfile.write("%s, %s\n" % (key, data[key]))
                    response = FileResponse(open('response.csv', 'rb'))
            if request.headers['Accept'] == 'application/json':
                response = JsonResponse(data, safe=False)
        return response
    else:
        return HttpResponseNotFound('<h1>Page not found</h1>')
